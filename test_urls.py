def test_index(client):
    response = client.get('/')

    assert b'Index Page' in response.get_data()


def test_hello(client):
    response = client.get('/hello')

    assert b'Hello, World' in response.get_data()


def test_show_user_profile(client):
    response = client.get('/user/skpaik')

    assert b'User skpaik' in response.get_data()
