import pytest

from hello import app


@pytest.fixture()
def client():
    return app.test_client()
